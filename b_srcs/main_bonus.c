/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/06 18:54:42 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/23 23:08:11 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static void	open_and_exec(int *pid, int *pipefd, char *argv[],
	t_stuff stuff)
{
	int	infd;

	*pid = fork();
	if (*pid == -1)
		error_exit("ERROR\nfailed to create a new process", stuff);
	if (*pid == 0)
	{
		close(pipefd[0]);
		if (ft_strncmp(argv[1], "here_doc", 9) == 0)
			get_here_doc(argv[2]);
		infd = open(argv[1], O_RDONLY);
		if (infd < 0)
			error_exit(argv[1], stuff);
		if (dup2(infd, STDIN_FILENO) == -1)
			error_exit("ERROR\ncould not duplicate file-descriptor",
				stuff);
		close(infd);
		if (dup2(pipefd[1], STDOUT_FILENO) == -1)
			error_exit("ERROR\ncould not duplicate file-descriptor",
				stuff);
		close(pipefd[1]);
		execve(stuff.cmmnd_arrays[0][0], stuff.cmmnd_arrays[0], NULL);
	}
	close(pipefd[1]);
}

static int	exec_cmmnd(int *readfd,
	t_stuff stuff, int arr)
{
	int	pipefd[2];
	int	pid;

	if (pipe(pipefd) == -1)
		error_exit("ERROR\nfailed to create a pipe", stuff);
	pid = fork();
	if (pid == -1)
		error_exit("ERROR\nfailed to create a new process", stuff);
	if (pid == 0)
	{
		if (dup2(*readfd, STDIN_FILENO) == -1)
			error_exit("ERROR\ncould not duplicate file-descriptor",
				stuff);
		close(pipefd[0]);
		if (dup2(pipefd[1], STDOUT_FILENO) == -1)
			error_exit("ERROR\ncould not duplicate file-descriptor",
				stuff);
		close(pipefd[1]);
		execve(stuff.cmmnd_arrays[arr][0], stuff.cmmnd_arrays[arr], NULL);
	}
	close(*readfd);
	close(pipefd[1]);
	*readfd = pipefd[0];
	return (pid);
}

static void	exec_and_write(int *readfd, char *argv[],
	t_stuff stuff, int arr)
{
	int	outfd;

	if (ft_strncmp(argv[1], "here_doc", 9) == 0)
		outfd = open(argv[arr + 4], O_WRONLY | O_CREAT | O_APPEND, 00777);
	else
		outfd = open(argv[arr + 3], O_WRONLY | O_CREAT | O_TRUNC, 00777);
	if (outfd < 0)
		error_exit("ERROR\ncould not open the output-file", stuff);
	if (dup2(*readfd, STDIN_FILENO) == -1)
		error_exit("ERROR\ncould not duplicate file-descriptor", stuff);
	close(*readfd);
	if (dup2(outfd, STDOUT_FILENO) == -1)
		error_exit("ERROR\ncould not duplicate file-descriptor", stuff);
	close(outfd);
	execve(stuff.cmmnd_arrays[arr][0], stuff.cmmnd_arrays[arr], NULL);
}

static void	pipex(int argc, char *argv[], t_stuff stuff)
{
	int	pipefd[2];
	int	readfd;
	int	i;

	i = 0;
	if (pipe(pipefd) == -1)
		error_exit("ERROR\nfailed to create a pipe", stuff);
	open_and_exec(&stuff.pid[0], pipefd, argv, stuff);
	readfd = pipefd[0];
	while (i++ < argc - 5)
		stuff.pid[i] = exec_cmmnd(&readfd, stuff, i);
	stuff.pid[i] = fork();
	if (stuff.pid[i] == -1)
		error_exit("ERROR\nfailed to create a new process", stuff);
	if (stuff.pid[i] == 0)
		exec_and_write(&readfd, argv, stuff, i++);
	i = -1;
	while (i++ < argc - 1)
		waitpid(stuff.pid[i], NULL, 0);
	free(stuff.pid);
}

int	main(int argc, char *argv[], char *envp[])
{
	t_stuff	stuff;
	int		i;

	i = 0;
	stuff = allocate_stuff(argc, argv);
	if (ft_strncmp(argv[1], "here_doc", 9) == 0)
		argc--;
	while (i++ < argc - 3)
	{
		if (ft_strncmp(argv[1], "here_doc", 9) == 0)
			get_cmmnd_array(argv[i + 2], envp, stuff, i - 1);
		else
			get_cmmnd_array(argv[i + 1], envp, stuff, i - 1);
		if (!stuff.cmmnd_arrays[i - 1])
			error_exit("ERROR\nerror with an argument", stuff);
	}
	stuff.cmmnd_arrays[argc - 3] = NULL;
	pipex(argc, argv, stuff);
	free_all_arrays(stuff.cmmnd_arrays);
	unlink("here_doc");
	return (0);
}
