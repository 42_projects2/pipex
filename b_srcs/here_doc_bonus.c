/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   here_doc_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/20 12:11:56 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/22 15:44:46 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"
#include "../get_next_line/get_next_line.h"

void	get_here_doc(char *limiter)
{
	int		here_doc_fd;
	char	*line;

	here_doc_fd = open("here_doc", O_WRONLY | O_CREAT | O_TRUNC, 00777);
	while (1)
	{
		write(1, "pipe heredoc> ", 14);
		line = get_next_line(STDIN_FILENO);
		if (!line || (ft_strncmp(line, limiter, ft_strlen(line) - 1) == 0
				&& ft_strlen(line) - 1 == ft_strlen(limiter)))
			break ;
		write(here_doc_fd, line, ft_strlen(line));
		free(line);
	}
	free(line);
}
