/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/07 18:13:59 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/20 16:47:46 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include <unistd.h>
# include <stdio.h>
# include <string.h>
# include <fcntl.h>
# include <sys/wait.h>
# include <errno.h>
# include "../ft_printf/ft_printf.h"

typedef struct s_stuff
{
	char	***cmmnd_arrays;
	int		*pid;
}				t_stuff;

// main_bonus
int		main(int argc, char *argv[], char *envp[]);
// get_cmmnd_array_bonus
void	get_cmmnd_array(char *cmmnd, char *envp[],
			t_stuff stuff, int i);
// free_and_error_bonus
void	free_array(char **array);
void	free_all_arrays(char ***arrays);
void	error_exit(char *err_str, t_stuff stuff);
t_stuff	allocate_stuff(int argc, char *argv[]);
// here_doc_bonus
void	get_here_doc(char *limiter);

#endif