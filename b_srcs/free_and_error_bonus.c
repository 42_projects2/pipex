/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_and_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/18 14:31:58 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/18 14:33:33 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

t_stuff	allocate_stuff(int argc, char *argv[])
{
	t_stuff	stuff;

	if (argc < 5 || (ft_strncmp(argv[1], "here_doc", 9) == 0 && argc < 6))
	{
		ft_printf("ERROR\nnot enough arguments");
		exit(EXIT_FAILURE);
	}
	stuff.cmmnd_arrays = ft_calloc(argc - 2, sizeof(char **));
	stuff.pid = ft_calloc(argc, sizeof(int));
	if (!stuff.cmmnd_arrays || !stuff.pid)
		error_exit("ERROR\nmemory allocation failed", stuff);
	return (stuff);
}

void	free_array(char **array)
{
	int	i;

	i = -1;
	if (!array)
		return ;
	while (array[++i])
		free(array[i]);
	free(array);
}

void	free_all_arrays(char ***arrays)
{
	int	i;

	i = -1;
	if (!arrays)
		return ;
	while (arrays[++i])
		free_array(arrays[i]);
	free(arrays);
}

void	error_exit(char *err_str, t_stuff stuff)
{
	free(stuff.pid);
	free_all_arrays(stuff.cmmnd_arrays);
	if (errno == 0)
		ft_printf("%s\n", err_str);
	else
		perror(err_str);
	exit(EXIT_FAILURE);
}
