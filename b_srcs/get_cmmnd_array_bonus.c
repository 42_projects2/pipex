/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_cmmnd_array.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/07 18:10:35 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/18 14:45:04 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static char	**split_env_paths(char *envp[])
{
	char	**paths;
	int		i;

	i = -1;
	while (envp[++i])
	{
		if (ft_strncmp(envp[i], "PATH=", 5) == 0)
		{
			paths = ft_split(envp[i] + 5, ':');
			if (!paths)
				return (NULL);
		}
	}
	return (paths);
}

static char	*check_path(char *path, char *cmmnd)
{
	char	*s1;
	char	*s2;

	s1 = ft_strjoin(path, "/");
	s2 = ft_strjoin(s1, cmmnd);
	if (s1)
		free(s1);
	if (!s2)
		return (NULL);
	if (access(s2, X_OK) == 0)
		return (s2);
	free(s2);
	return (NULL);
}

static char	*get_path(char *cmmnd, char *envp[])
{
	int		i;
	char	**paths;
	char	*cmmnd_path;

	paths = split_env_paths(envp);
	if (!paths)
		return (NULL);
	i = -1;
	while (paths[++i])
	{
		cmmnd_path = check_path(paths[i], cmmnd);
		if (cmmnd_path)
		{
			free_array(paths);
			return (cmmnd_path);
		}
	}
	free_array(paths);
	return (NULL);
}

static int	isempty(char *str)
{
	int	i;

	if (!str)
		return (1);
	i = -1;
	while (str[++i])
	{
		if (str[i] != '\t' && str[i] != '\n' && str[i] != '\v'
			&& str[i] != '\f' && str[i] != '\r' && str[i] != ' ')
			return (0);
	}
	return (1);
}

void	get_cmmnd_array(char *cmmnd, char *envp[],
	t_stuff stuff, int i)
{
	char	*path;
	char	*temp;

	stuff.cmmnd_arrays[i] = ft_split(cmmnd, ' ');
	if (isempty(cmmnd) || !stuff.cmmnd_arrays[i])
		error_exit("ERROR\nerror with an argument", stuff);
	if (envp[0] == NULL)
	{
		if (access(stuff.cmmnd_arrays[i][0], X_OK) != 0)
			error_exit("ERROR\nerror with an argument", stuff);
		return ;
	}
	if (access(stuff.cmmnd_arrays[i][0], X_OK) == 0)
		return ;
	path = stuff.cmmnd_arrays[i][0];
	temp = get_path(stuff.cmmnd_arrays[i][0], envp);
	if (temp)
		stuff.cmmnd_arrays[i][0] = temp;
	else
		error_exit("ERROR\nerror with an argument", stuff);
	free(path);
	return ;
}
