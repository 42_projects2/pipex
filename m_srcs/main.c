/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/06 18:54:42 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/23 11:16:50 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static void	open_and_exec(int *pipefd, char *argv[],
	char ***cmmnd_array, int arr)
{
	int	infd;

	close(pipefd[0]);
	infd = open(argv[1], O_RDONLY);
	if (infd < 0)
		error_exit(argv[1], cmmnd_array);
	if (dup2(infd, STDIN_FILENO) == -1)
		error_exit("ERROR\ncould not duplicate file-descriptor", cmmnd_array);
	close(infd);
	if (dup2(pipefd[1], STDOUT_FILENO) == -1)
		error_exit("ERROR\ncould not duplicate file-descriptor", cmmnd_array);
	close(pipefd[1]);
	execve(cmmnd_array[arr][0], cmmnd_array[arr], NULL);
}

static void	exec_and_write(int *pipefd, char *argv[],
	char ***cmmnd_array, int arr)
{
	int	outfd;

	close(pipefd[1]);
	outfd = open(argv[4], O_WRONLY | O_CREAT | O_TRUNC, 00777);
	if (outfd < 0)
		error_exit("ERROR\ncould not open the output-file", cmmnd_array);
	if (dup2(pipefd[0], STDIN_FILENO) == -1)
		error_exit("ERROR\ncould not duplicate file-descriptor", cmmnd_array);
	close(pipefd[0]);
	if (dup2(outfd, STDOUT_FILENO) == -1)
		error_exit("ERROR\ncould not duplicate file-descriptor", cmmnd_array);
	close(outfd);
	execve(cmmnd_array[arr][0], cmmnd_array[arr], NULL);
}

static void	pipex(char *argv[], char ***cmmnd_arrays)
{
	int	pipefd[2];
	int	pid1;
	int	pid2;

	if (pipe(pipefd) == -1)
		error_exit("ERROR\nfailed to create a pipe", cmmnd_arrays);
	pid1 = fork();
	if (pid1 == -1)
		error_exit("ERROR\nfailed to create a new process", cmmnd_arrays);
	if (pid1 == 0)
		open_and_exec(pipefd, argv, cmmnd_arrays, 0);
	close(pipefd[1]);
	pid2 = fork();
	if (pid2 == -1)
		error_exit("ERROR\nfailed to create a new process", cmmnd_arrays);
	if (pid2 == 0)
		exec_and_write(pipefd, argv, cmmnd_arrays, 1);
	close(pipefd[0]);
	waitpid(pid1, NULL, 0);
	waitpid(pid2, NULL, 0);
}

int	main(int argc, char *argv[], char *envp[])
{
	char	***cmmnd_arrays;
	int		i;

	if (argc != 5)
		error_exit("ERROR\nwrong number of arguments", NULL);
	i = 0;
	cmmnd_arrays = ft_calloc(argc - 2, sizeof(char **));
	if (!cmmnd_arrays)
		error_exit("ERROR\nmemory allocation failed", cmmnd_arrays);
	while (i++ < argc - 3)
	{
		get_cmmnd_array(argv[i + 1], envp, cmmnd_arrays, i - 1);
		if (!cmmnd_arrays[i - 1] || !cmmnd_arrays[i - 1][0])
			error_exit("ERROR\nerror with an argument", cmmnd_arrays);
	}
	cmmnd_arrays[argc - 3] = NULL;
	pipex(argv, cmmnd_arrays);
	free_all_arrays(cmmnd_arrays);
	return (0);
}
