/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_and_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/18 14:31:58 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/18 14:33:33 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

void	free_array(char **array)
{
	int	i;

	i = -1;
	if (!array)
		return ;
	while (array[++i])
		free(array[i]);
	free(array);
}

void	free_all_arrays(char ***arrays)
{
	int	i;

	i = -1;
	if (!arrays)
		return ;
	while (arrays[++i])
		free_array(arrays[i]);
	free(arrays);
}

void	error_exit(char *err_str, char ***arrays)
{
	free_all_arrays(arrays);
	if (errno == 0)
		ft_printf("%s\n", err_str);
	else
		perror(err_str);
	exit(EXIT_FAILURE);
}
