/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/07 18:13:59 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/18 14:49:29 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include <unistd.h>
# include <stdio.h>
# include <string.h>
# include <fcntl.h>
# include <sys/wait.h>
# include <errno.h>
# include "../ft_printf/ft_printf.h"

// main
int			main(int argc, char *argv[], char *envp[]);
// get_cmmnd_array
void		get_cmmnd_array(char *cmmnd, char *envp[],
				char ***cmmnd_arrays, int i);
// free_and_error
void		free_array(char **array);
void		free_all_arrays(char ***arrays);
void		error_exit(char *err_str, char ***arrays);

#endif