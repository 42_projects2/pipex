# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/08/06 18:55:13 by tfriedri          #+#    #+#              #
#    Updated: 2022/08/23 20:41:12 by tfriedri         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = pipex
BONUS = pipex_bonus
CC = gcc
CFLAGS = -Wall -Werror -Wextra -D BUFFER_SIZE=1 -g
LDFLAGS = -g
FTPRINTF = ./ft_printf/libftprintf.a
FTPRINTFDIR = ./ft_printf

M_SRCS =	./m_srcs/main.c \
			./m_srcs/get_cmmnd_array.c \
			./m_srcs/free_and_error.c

B_SRCS =	./b_srcs/main_bonus.c \
			./b_srcs/get_cmmnd_array_bonus.c \
			./b_srcs/free_and_error_bonus.c \
			./b_srcs/here_doc_bonus.c \
			./get_next_line/get_next_line.c \
			./get_next_line/get_next_line_utils.c \

M_OBJS = $(M_SRCS:.c=.o)

B_OBJS = $(B_SRCS:.c=.o)

all: $(NAME)

bonus: $(BONUS)

$(FTPRINTF):
	@make -C $(FTPRINTFDIR)

$(NAME): $(FTPRINTF) $(M_OBJS)
	@$(CC) $(M_OBJS) -L$(FTPRINTFDIR) -lftprintf -o $(NAME)
#-fsanitize=address

$(BONUS): $(FTPRINTF) $(B_OBJS)
	@$(CC) $(B_OBJS) -L$(FTPRINTFDIR) -lftprintf -o $(BONUS)
#-fsanitize=address

clean:
	@rm -f $(M_OBJS)
	@rm -f $(B_OBJS)
	@cd $(FTPRINTFDIR) && make clean

fclean: clean
	@rm -f $(NAME)
	@rm -f $(BONUS)
	@cd $(FTPRINTFDIR) && make fclean

re:
ifeq ($(shell test -s pipex && echo yes),yes)
ifeq ($(shell test -s pipex_bonus && echo yes),yes)
	@make fclean
	@make all
	@make bonus
else
	@make fclean
	@make all
endif
else ifeq ($(shell test -s pipex_bonus && echo yes),yes)
		@make fclean
		@make bonus
else
	@make all
endif
